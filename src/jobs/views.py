import itertools
import json

import requests
from django.db import transaction
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from jobs.filters import JobFilter
from jobs.models import Jobs
from jobs.serializers import JobsSerializer

# Create your views here.
# TODO
# 1. create models - Done
# 2. create test cases
#     2.1 create factory
# 3. create api - Done
# 4. create serializers - Done
# 5. create views - Done
# 6. load jobs - Done
# 7. create filters - Done

JOB_URL = "https://jobs.github.com/positions.json?page="
SUCCESS_MESSAGE = {"message": "Job data sync successfully", "status_code": status.HTTP_200_OK}


class JobsViewSet(ListModelMixin,GenericViewSet):
    queryset = Jobs.objects.all()
    serializer_class = JobsSerializer
    filter_class = JobFilter

    @transaction.atomic
    @action(
        methods=["post"],
        detail=False,
        url_path='sync',
        url_name='sync'
    )
    def sync(self, request):
        response = SUCCESS_MESSAGE

        for i in itertools.count(1):
            req = requests.get(JOB_URL + str(i))
            try:
                # when empty data returns
                if not json.loads(req.text):
                    break
                else:
                    job_data = json.loads(req.text)
                    Jobs.populate_job_data(job_data)
            except Exception as e:
                response = {"message": str(e), "status_code": status.HTTP_400_BAD_REQUEST}
            i += 1
        return Response(response, status.HTTP_200_OK)
