# Create your tests here.
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from jobs.factory import JobFactory


class JobsApiTestCases(APITestCase):
    def setUp(self):
        self.job_software_development_1 = JobFactory(title="Software", location="Chennai")
        self.job_software_development_2 = JobFactory(title="Software", location="Chennai")
        self.job_network = JobFactory(title="Network", location="Bangalore")

    def test_get_all_jobs(self):
        url = reverse('jobs-list')
        response = self.client.get(url)
        self.assertEquals(3,response.data['count'])

    def test_get_only_chennai_jobs(self):
        url = "%s?location=chennai" % reverse('jobs-list')
        response = self.client.get(url)
        self.assertEquals(2,response.data['count'])

    def test_get_only_networking_job(self):
        url = "%s?title=network" % reverse('jobs-list')
        response = self.client.get(url)
        self.assertEquals(1,response.data['count'])
