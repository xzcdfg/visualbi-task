import uuid

from django.db import models


class Jobs(models.Model):
    id = models.UUIDField(
        editable=False,
        primary_key=True,
        default=uuid.uuid4
    )
    uuid = models.UUIDField(null=True, blank=True, help_text="Job reference id of github")
    type = models.CharField(null=True, blank=True, max_length=200, help_text="type of Job")
    url = models.CharField(null=True, blank=True, max_length=200, help_text="Job detail")
    created_at = models.CharField(null=True, blank=True, max_length=200, help_text="Job created time")
    company = models.CharField(null=True, blank=True, max_length=200,help_text="Comapny name")
    company_url = models.CharField(null=True, blank=True, max_length=200, help_text="Company url")
    location = models.CharField(null=True, blank=True, max_length=200, help_text="Company location")
    title = models.CharField(null=True, blank=True, max_length=200, help_text="Job Title")
    description = models.TextField(null=True, blank=True, help_text="Job description")
    how_to_apply = models.TextField(null=True, blank=True, help_text="How to apply for the job")
    company_logo = models.CharField(null=True, blank=True, max_length=300, help_text="Company logo url")

    @classmethod
    def populate_job_data(cls, job_data):
        for job in job_data:
            job_uuid = job.pop("id")
            job, is_created = Jobs.objects.update_or_create(uuid=job_uuid, defaults=job)

