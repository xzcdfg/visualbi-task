import django_filters

from jobs.models import Jobs


class JobFilter(django_filters.FilterSet):
    type = django_filters.filters.CharFilter(method="get_type")
    company = django_filters.filters.CharFilter(method="get_company")
    location = django_filters.filters.CharFilter(method="get_location")
    title = django_filters.filters.CharFilter(method="get_title")

    class Meta:
        model = Jobs
        fields = ['type', 'company', 'location', 'title']

    def get_type(self, queryset, name, value):
        return queryset.filter(type__contains=value)

    def get_company(self, queryset, name, value):
        return queryset.filter(company__contains=value)

    def get_location(self, queryset, name, value):
        return queryset.filter(location__contains=value)

    def get_title(self, queryset, name, value):
        return queryset.filter(title__contains=value)


