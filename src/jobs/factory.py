from factory import DjangoModelFactory
from factory.fuzzy import FuzzyText

from jobs.models import Jobs


class JobFactory(DjangoModelFactory):
    class Meta:
        model = Jobs

    title = FuzzyText()
    location = "Chennai"