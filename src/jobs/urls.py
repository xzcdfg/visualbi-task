from rest_framework.routers import SimpleRouter

from jobs.views import JobsViewSet

router = SimpleRouter()
router.register("jobs", JobsViewSet)

urlpatterns = router.urls